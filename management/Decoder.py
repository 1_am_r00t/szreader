from gurux_dlms import GXDLMSTranslator, TranslatorOutputType, GXByteBuffer
import logging

logger = logging.getLogger(__name__)


def decode(key: str, message: str) -> str:
    logger.debug("Decrypting message \"{}\" with key \"{}\" ...".format(message, key))
    decrypt = GXDLMSTranslator(TranslatorOutputType.SIMPLE_XML)
    decrypt.comments = True
    decrypt.blockCipherKey = GXByteBuffer.hexToBytes(key)
    msg = decrypt.messageToXml(GXByteBuffer.hexToBytes(message))
    logger.debug("Decryption finished successfully")
    return msg
