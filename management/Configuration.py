import configparser
import logging

logger = logging.getLogger(__name__)


class Configuration:
    def __init__(self, config_file=None):
        """
        Args:
            config_file: Configurationfile to be read from disk, defaults to controller.DEFAULT_CONFIG_FILE
        """
        logger.debug("Initializing Configuration with configfile {}".format(config_file))

        self._config_file = config_file
        self._parser = configparser.ConfigParser()
        self._read_configuration()
        logger.info("Configuration initialized with file {}".format(str(self._config_file.resolve())))

    def __getitem__(self, item):
        return self._parser[item]

    @property
    def configuration(self):
        return self._parser

    def _read_configuration(self):

        if self._config_file is None:
            raise IOError("No configfile supplied, exiting.")

        if self._config_file.exists():
            self._parser.read(str(self._config_file.resolve()))
            logging.debug("Configuration read from configfile {}".format(self._config_file.resolve()))
        else:
            raise IOError("Unable to access configuration file {}".format(self._config_file.resolve()))
