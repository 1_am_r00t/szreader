import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="szreader",
    version="0.1.0-alpha2",
    author="Alexander Strnad",
    author_email="alexanderstrnad@gmail.com",
    description="Utility to read data from smart energy meters and publish results to configurable datasinks",
    license="GPLv2",
    keywords="smartmeter",
#    url="http://packages.python.org/an_example_pypi_project",
    packages=['datasource', 'datasink', 'management'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
        "Operating System :: POSIX :: Linux",
        "Operating System :: Microsoft :: Windows",
        "Topic :: System :: Logging",
        "Topic :: System :: Monitoring"
    ],
    install_requires=["jinja2", "gurux_dlms", "gurux_common", "pytz", "tzlocal", "python-dateutil", "requests", "paho-mqtt", "pyserial"],
)
