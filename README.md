# SZReader Project
A configurable python project which reads encrypted DMLS packages from smartmeters deployed in Austria (and possibly elsewhere too!), decrypts the content and forwards relevant data to its datasinks.
## Intended use
Installation on hardware with connection to a smartmeter (via serial connection, mqtt or plain text files so far)
## Installation
Pull project, install "setuptools" and "wheel", then install requirements and run szreader.py with intended paramters (see below)
### Requirements
Install the required packages to run via ```pip install -r requirements.txt``` in the project directory
### Runtime parameters
```
"-l", "--logfile", Logfile to use, defaults to "szreader.log"
"-v", "--loglevel", Loglevel for logging, defaults to "INFO", choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
"-n", "--nolog", Turn off logging, helpful for embedded systems
"-c", "--configfile", Configurationfile to use, defaults to "szreader.config"
```

## Installation using Docker (best when using MQTT as source & sink)
Pull/clone project, ```cd szreader```, ```cp szreader.config.example szreader.config```, modify config as required, ```docker build -t szreader .```, ```docker run -d -v ./:/app --restart unless-stopped szreader```
