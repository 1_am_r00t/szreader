from datasink.IDataSink import IDataSink
from management.PublishingData import PublishingData
from jinja2 import Template
import requests
import logging
import json

logger = logging.getLogger(__name__)


class HttpDataSink(IDataSink):

    def __init__(self, config):
        super().__init__(config)
        logger.info("Initialized HttpDataSource {}".format(self._config.name))

        self._host = self._config.get("host")
        self._method = self._config.get("method")
        self._template = self._config.get("template")

    def send_data(self, data: PublishingData):
        if self._template is not None:
            with open(self._template) as file:
                json_data = self._fill_template(file.read(), data)
        else:
            json_data = "{}"
        url = self._fill_template(self._host, data)
        json_data = json.loads(json_data)
        post_headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        result = None
        try:
            if self._method == "POST":
                logger.info("POSTing data {} to url {}".format(repr(json_data), repr(url)))
                result = requests.post(url, data=json.dumps(json_data), headers=post_headers)
            elif self._method == "GET":
                logger.info("Requesting URL {}".format(url))
                result = requests.get(url)
            else:
                logger.warning("No HTTP method configured, not sending any data")
            logger.debug("Server response: {}".format(repr(result)))
        except Exception:
            logger.exception("Error while sending data to {}".format(url))

    def _fill_template(self, template, data):
        tm = Template(template)
        return tm.render(current_counter_in=data.current_counter_in, current_counter_out=data.current_counter_out,
                         current_power_in=data.current_power_in, current_power_out=data.current_power_out, timestamp=data.timestamp)
