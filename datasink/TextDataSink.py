from datasink.IDataSink import IDataSink
from management.PublishingData import PublishingData
import logging

logger = logging.getLogger(__name__)


class TextDataSink(IDataSink):

    def send_data(self, data: PublishingData):
        logger.info("Writing data: {}".format(repr(data)))
        with open(self._config.get("path"), 'a') as file:
            file.write("{}\n".format(repr(data)))
            file.flush()

