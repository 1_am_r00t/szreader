import logging
import json
import paho.mqtt.client as mqtt

from jinja2 import Template
from datasink.IDataSink import IDataSink
from management.PublishingData import PublishingData

logger = logging.getLogger(__name__)


class MqttDataSink(IDataSink):

    def __init__(self, config):
        super().__init__(config)
        self._host = self._config.get("host")
        self._port = int(self._config.get("port", 1883))
        self._qos = int(self._config.get("qos", 0))
        self._retain = int(self._config.get("retain", 0))
        self._topics = json.loads(self._config.get("topics"))
        self._wtopic = self._config.get("wtopic")
        self._wpayload = self._config.get("wpayload")
        self._wqos = int(self._config.get("wqos", 0))
        self._wretain = int(self._config.get("wretain", 0))
        self._client_name = self._config.get("clientname", "szreader_" + self._config.name)
        logger.info("Initialized MqttDataSink {}".format(self._config.name))

        try:
            self._client = mqtt.Client(self._client_name)
            self._client.username_pw_set(username=self._config.get("username", None), password=self._config.get("password", None))
            self._client.will_set(self._wtopic, payload=self._wpayload, qos=self._wqos, retain=self._wretain)
            self._client.enable_logger(logger=logger)
            self._client.connect(host=self._host, port=self._port)
            logger.info("Connected to MQTT Server {} on port {}".format(self._host, self._port))
            self._client.loop_start()
        except:
            logger.exception("Error in MQTT connection")

    def stop_sink(self):
        if self._client.is_connected():
            logger.info("Shutting down MqttDataSink {}...".format(self._config.name))
            self._client.loop_stop()
            self._client.disconnect()
            logger.info("MqttDataSink {} shutdown complete.".format(self._config.name))

    def __del__(self):
        self.stop_sink()

    def send_data(self, data: PublishingData):
        if self._client.is_connected():
            for topic, value_template in self._topics.items():
                value = Template(value_template).render(timestamp=data.timestamp, current_counter_in=data.current_counter_in, current_counter_out=data.current_counter_out, current_power_in=data.current_power_in, current_power_out=data.current_power_out)
                self._client.publish(topic=topic, payload=value, qos=self._qos, retain=self._retain)
                logger.debug("Published data \"{}\" to host {} on topic {} as client {}".format(value, self._host, topic, self._client_name))
        else:
            logger.error("MQTT Client {} not connected, not able to send data".format(self._client_name))
