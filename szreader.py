#!/usr/bin/env python
import argparse
import logging
import os
import signal
import traceback
from logging import handlers
from pathlib import Path

import sys
from time import sleep

from management import DEFAULT_CONFIG_FILE
from management.Controller import Controller


class ServiceExit(Exception):
    """
    Custom exception which is used to trigger the clean exit
    of all running threads and the main program.
    """
    pass


def service_shutdown(signum, frame):
    raise ServiceExit


def main():
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)
    arg_parse = argparse.ArgumentParser()
    arg_parse.add_argument("-l", "--logfile", help="Logfile to use", default="szreader.log")
    arg_parse.add_argument("-v", "--loglevel", help="Loglevel for logging", default="INFO",
                           choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"])
    arg_parse.add_argument("-n", "--nolog", help="Turn off logging, helpful for embedded systems", action="store_true")
    arg_parse.add_argument("-c", "--configfile", help="Configurationfile to use", default=DEFAULT_CONFIG_FILE)

    args = arg_parse.parse_args()

    called_script_dir = Path(os.path.abspath(os.path.dirname(sys.argv[0])))
    # Setup logging
    # Create Logger
    logger = logging.getLogger()
    if args.nolog:
        handler = logging.NullHandler()
    else:
        logfile = Path(args.logfile)

        if not logfile.is_absolute():
            logfile = called_script_dir / args.logfile

        # Rotate logfiles, keep 5 days
        handler = handlers.TimedRotatingFileHandler(str(logfile), when="midnight", backupCount=5, encoding="UTF-8")

        # Set logging format to something meaningful
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)

    logger.addHandler(handler)
    logger.setLevel(args.loglevel)
    cont = None
    try:
        logger.info("SZReader initializing...")
        logger.debug("SZReader script called in directory \"{}\"".format(called_script_dir))

        ###
        # Cases:
        # 1) config_file is just a filename ("szreader.config")
        #    - Attempt in CWD
        #    - Attempt in directory of called script (szreader.py)
        # 2) config_file is relative path ("szreader/szreader.config")
        #    - Attempt in CWD
        #    - Attempt in merged path with called script (called scripts parent / config_file)
        # 3) config_file is an absolute path
        ###

        # Get the full path of the called script
        called_script_dir = Path(os.path.abspath(os.path.dirname(sys.argv[0])))
        file = cf = Path(args.configfile)
        logger.debug("Trying to locate configfile \"{}\"...".format(args.configfile))
        if not file.exists():
            logger.debug("Configfile not found, trying in \"{}\"".format(called_script_dir))
            # Look for config file in directory of executed script
            file = called_script_dir / cf
            # Found the config file there?
            if not file.exists():
                logger.debug("Configfile not found at \"{}\"".format(file))
                # Look for config file relative to current working directory
                file = Path.cwd() / cf
                logger.debug("Setting configfile to location \"{}\"".format(file))
                if not file.exists():
                    logger.critical("Configurationfile not found, exiting.")
                    sys.exit(1)

        cont = Controller(file)
        logger.info("SZReader starting...")
        cont.start()
        while cont.any_source_running():
            sleep(1)
        logger.info("All sources have shut down, exiting.")
    except ServiceExit:
        logger.info("Caught interrupt, shutting down SZReader...")
        if cont:
            cont.shutdown()
        logger.info("SZReader shutdown complete")

    except Exception:
        logger.exception("Error in SZReader application")
        traceback.print_exc(file=sys.stdout)
    sys.exit(0)


if __name__ == "__main__":
    main()
