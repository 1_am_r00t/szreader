from unittest import TestCase

from management.MessageMangler import MessageMangler
from management.PublishingData import PublishingData


class ManglerTest(TestCase):
    def setUp(self) -> None:
        self._ok_message_file = "test_data.xml"
        self._nok_message = "Anything but formatted XML with our needed tags"

    def test_mangle_nok(self):
        man = MessageMangler("Test", self._nok_message)
        self.assertFalse(man.is_data_valid())

    def test_mangle_ok(self):
        with open(self._ok_message_file) as f:
            man = MessageMangler("Test", f.read())
            self.assertIsInstance(man.get_publishing_data(), PublishingData)
            self.assertTrue(man.is_data_valid())
